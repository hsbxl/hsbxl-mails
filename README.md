HSBXL mail templates

THose templates are used by Concierge. Placeholders are shell variables to be substituted by envsubst

Example:

$ FIRSTNAME='john'
$ QUORUM=10
$ export FIRSTNAME QUORUM
$ cat generalassembly.txt | envsubst